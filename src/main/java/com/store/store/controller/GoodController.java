package com.store.store.controller;


import com.store.store.entity.Categories;
import com.store.store.entity.Comments;
import com.store.store.entity.Good;
import com.store.store.entity.User;
import com.store.store.service.CategoriesService;
import com.store.store.service.CommentService;
import com.store.store.service.GoodService;
import com.store.store.service.UserService;
import com.store.store.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;

@Controller
public class GoodController {

    @Autowired
    GoodService goodService;
    @Autowired
    CategoriesService categoriesService;

    @Autowired
    CommentService commentService;
    @Autowired
    UserServiceImpl userService;

    @RequestMapping(value = "/goodDetails",method = RequestMethod.POST)
    public ModelAndView getGoodDetails(@RequestParam int goodID, @RequestParam String asname)
    {


        Good good = goodService.goodById(goodID);
        List<Comments> comments = commentService.commentByGood(goodID);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("goodDetails");
        modelAndView.addObject("good",good);
        modelAndView.addObject("comments", comments);
        modelAndView.addObject("asname",asname);
        return modelAndView;
    }

    @RequestMapping(value = "/goodsByCategory",method = RequestMethod.POST)
    public ModelAndView getGoodsByCategory(@RequestParam int categoriesID,@RequestParam String asname)
    {
        List<Good> list = goodService.goodByCategories(categoriesID);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("goodsByCategory");
        modelAndView.addObject("categoriesID",categoriesID);
        modelAndView.addObject("goods",list);
        modelAndView.addObject("asname",asname);
        return modelAndView;
    }

    @RequestMapping(value = "good/edit", method = RequestMethod.POST)
    public ModelAndView getEditGood(@RequestParam int categoriesID, @RequestParam String asname)
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("editGood");
        modelAndView.addObject("categoriesID", categoriesID);
        modelAndView.addObject("asname",asname);
        return modelAndView;
    }

    @RequestMapping(value = "good/edit/result",method = RequestMethod.POST)
    public ModelAndView getEditGoodResult(@RequestParam int categoriesID, String name, int price, String description, @RequestParam String asname)
    {
        Categories categories  = categoriesService.categoryByID(categoriesID);
        goodService.addGood(name, price, description, categories);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("resultEditGood");
        modelAndView.addObject("categoriesID", categoriesID);
        modelAndView.addObject("asname",asname);
        return modelAndView;
    }

    @RequestMapping(value = "comment",method = RequestMethod.POST)
    public ModelAndView editComment(@RequestParam String comment, int goodID, @RequestParam String asname)
    {
        commentService.addComment(comment, goodID);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("comment");
        modelAndView.addObject("goodID", goodID);
        modelAndView.addObject("asname",asname);
        return modelAndView;
    }

}
