package com.store.store.controller;

import com.store.store.entity.Good;
import com.store.store.entity.User;
import com.store.store.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class MainController {

    @Autowired
    GoodService goodService;



    @RequestMapping(value = "main", method = RequestMethod.GET)
    public ModelAndView getMain()
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        List<Good> list = goodService.allGoods();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("main");
        modelAndView.addObject("goods",list);
        modelAndView.addObject("asname", name);

        return modelAndView;
    }
}
