package com.store.store.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

import static org.apache.tomcat.jdbc.pool.ConnectionPool.getStackTrace;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    @ExceptionHandler(Throwable.class)
    public ModelAndView genericExceptionHandler(Exception ex){

        String errorMessage = ex.getMessage();
        String errorStacktrace = getStackTrace(ex);
        ModelAndView mv = new ModelAndView("errors/error");
        Map<String, Object> model = mv.getModelMap();
        model.put("errorMessage", errorMessage);
        model.put("stackTrace", errorStacktrace);
        return mv;
    }
}