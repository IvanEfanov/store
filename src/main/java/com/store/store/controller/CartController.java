package com.store.store.controller;


import com.store.store.entity.Good;
import com.store.store.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CartController {

    @Autowired
    GoodService goodService;

    @RequestMapping(value = "/cart",method = RequestMethod.POST)
    public ModelAndView getCart(@RequestParam int goodID)
    {
        Good good = goodService.goodById(goodID);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("cart");
        modelAndView.addObject("good", good);
        return modelAndView;

    }


}
