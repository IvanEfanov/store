package com.store.store.controller;

import com.store.store.entity.Categories;
import com.store.store.service.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class CategoriesController {

    @Autowired
    CategoriesService categoriesService;

    @RequestMapping(value = "/categories",method = RequestMethod.POST)
    public ModelAndView getCategoriesPage(@RequestParam String asname)
    {
        List<Categories> list = categoriesService.getCategoriesList();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("categories");
        modelAndView.addObject("categoriesList",list);
        modelAndView.addObject("asname",asname);
        return modelAndView;
    }

    @RequestMapping(value = "/categories/edit", method = RequestMethod.POST)
    public ModelAndView getCategoryEdit(@RequestParam String asname)
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("editCategory");
        modelAndView.addObject("asname",asname);
        return modelAndView;
    }

    @RequestMapping(value = "/categories/result", method = RequestMethod.POST)
    public ModelAndView getCategoryResult(@RequestParam String nameCategory, @RequestParam String asname)
    {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("resultCategory");
        categoriesService.addCategory(nameCategory);
        modelAndView.addObject("asname",asname);
        return modelAndView;
    }


}
