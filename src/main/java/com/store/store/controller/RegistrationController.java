package com.store.store.controller;

import com.store.store.entity.Good;
import com.store.store.service.GoodService;
import com.store.store.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class RegistrationController {

    @Autowired
    private UserService userService;
    @Autowired
    GoodService goodService;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String getRegistration()
    {
        return "registration";
    }

    @RequestMapping(value = "/auth", method = RequestMethod.GET)
    public String getAuth()
    {
        return "autorithation";
    }


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView getResultRegistration(@RequestParam String login, @RequestParam String password,
                                            @RequestParam String name, @RequestParam String surname,
                                           @RequestParam String email, @RequestParam String city,
                                           @RequestParam String address)
    {
        List<Good> list = goodService.allGoods();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        modelAndView.addObject("login",login);
        modelAndView.addObject("password", password);
        modelAndView.addObject("name", name);
        modelAndView.addObject("surname",surname);
        modelAndView.addObject("email",email);
        modelAndView.addObject("city",city);
        modelAndView.addObject("address",address);
        modelAndView.addObject("goods",list);
        userService.newUser(login, password, name, surname, email, city, address);

        return modelAndView;
    }

}
