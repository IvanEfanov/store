package com.store.store.controller;

import com.store.store.entity.Good;
import com.store.store.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class SecurityController {

    @Autowired
    GoodService goodService;

    @RequestMapping(value={"/","/login"}, method = RequestMethod.GET)
    public ModelAndView login(){
        ModelAndView modelAndView = new ModelAndView();
        List<Good> list = goodService.allGoods();
        modelAndView.setViewName("login");
        modelAndView.addObject("goods",list);
        return modelAndView;
    }



    @RequestMapping(value = "/j_spring_security_check",method = RequestMethod.POST)
    public ModelAndView getMain(@RequestParam String login, @RequestParam String password)
    {

        ModelAndView modelAndView= new ModelAndView();
        modelAndView.addObject("login", login);
        modelAndView.addObject("password", password);


        return modelAndView;
    }
}
