package com.store.store.service;

import com.store.store.entity.Categories;
import com.store.store.entity.Good;

import java.util.List;

public interface GoodService {

    List<Good> allGoods();

    Good goodById(int goodID);

    List<Good> goodByCategories(int categoriesID);

    void addGood(String name, int price, String description, Categories categories);

}
