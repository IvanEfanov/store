package com.store.store.service;

import com.store.store.entity.Categories;

import java.util.List;

public interface CategoriesService {

    List<Categories> getCategoriesList();

    void addCategory(String name);

    Categories categoryByID(int categoryID);
}
