package com.store.store.service;

import com.store.store.entity.Comments;

import java.util.List;

public interface CommentService {

    void addComment(String comment,int goodID);

    List<Comments> commentByGood(int goodID);
}
