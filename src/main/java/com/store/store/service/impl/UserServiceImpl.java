package com.store.store.service.impl;

import com.store.store.dao.UserDao;
import com.store.store.entity.User;
import com.store.store.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    UserDao userDao;

    public void newUser(String login, String password, String name, String surname, String email, String city, String address)
    {
        userDao.setUser(login, password, name, surname, email, city, address);
    }


    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        User user = userDao.findByLogin(login);

        String username = user.getLogin();
        String password = user.getPassword();
        return new org.springframework.security.core.userdetails.User(username, password ,true,
                true, true, true, Collections.emptyList());
    }
}
