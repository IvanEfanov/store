package com.store.store.service.impl;


import com.store.store.dao.CategoriesDao;
import com.store.store.entity.Categories;
import com.store.store.service.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoriesServiceImpl implements CategoriesService{

    @Autowired
    CategoriesDao categoriesDao;

    public List<Categories> getCategoriesList()
    {
        return categoriesDao.getCategories();
    }

    public void addCategory(String name)
    {
        categoriesDao.saveCategory(name);
    }

    public Categories categoryByID(int categoryID)
    {
        return categoriesDao.getCategoryByID(categoryID);
    }
}
