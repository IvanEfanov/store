package com.store.store.service.impl;


import com.store.store.dao.GoodDao;
import com.store.store.entity.Categories;
import com.store.store.entity.Good;
import com.store.store.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodServiceImpl implements GoodService{

    @Autowired
    GoodDao goodDao;

    public List<Good> allGoods()
    {
        return goodDao.getAllGoods();
    }
    public Good goodById(int goodID)
    {
        return goodDao.getGoodByID(goodID);
    }

    public List<Good> goodByCategories(int categoriesID)
    {
        return goodDao.goodByCategory(categoriesID);
    }


    public void addGood(String name, int price, String description, Categories categories)
    {
        goodDao.saveGood(name, price, description, categories);
    }

}
