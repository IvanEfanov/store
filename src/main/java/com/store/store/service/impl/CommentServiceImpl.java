package com.store.store.service.impl;


import com.store.store.dao.CommentDAO;
import com.store.store.entity.Comments;
import com.store.store.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService{

    @Autowired
    CommentDAO commentDAO;

    public List<Comments> commentByGood(int goodID)
    {
        return commentDAO.getCommentsByGood(goodID);
    }

    public void addComment(String comment, int goodID)
    {
        commentDAO.addNewComment(comment, goodID);
    }
}
