package com.store.store.service;

public interface UserService {

    void newUser(String login, String password, String name, String surname, String email, String city, String address);

}
