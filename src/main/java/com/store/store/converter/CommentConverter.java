package com.store.store.converter;

import com.store.store.dto.CommentsData;
import com.store.store.entity.Comments;

public interface CommentConverter {

    CommentsData converter(Comments comments);
}
