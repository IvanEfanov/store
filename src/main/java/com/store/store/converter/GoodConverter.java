package com.store.store.converter;

import com.store.store.dto.GoodData;
import com.store.store.entity.Good;

public interface GoodConverter {

    GoodData converter(Good good);
}
