package com.store.store.converter;

import com.store.store.dto.CategoriesData;
import com.store.store.entity.Categories;

public interface CategoriesConverter {

    CategoriesData converter(Categories categories);
}
