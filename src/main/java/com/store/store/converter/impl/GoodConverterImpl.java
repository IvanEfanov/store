package com.store.store.converter.impl;


import com.store.store.converter.GoodConverter;
import com.store.store.dto.GoodData;
import com.store.store.entity.Good;
import org.springframework.stereotype.Component;

@Component
public class GoodConverterImpl implements GoodConverter{

    public GoodData converter(Good good)
    {
        GoodData goodData = new GoodData();
        goodData.setGoodID(good.getGoodID());
        goodData.setName(good.getName());
        goodData.setPrice(good.getPrice());
        goodData.setDescription(good.getDescription());
        goodData.setCategories(good.getCategories());
        goodData.setComments(good.getComments());


        return goodData;

    }
}
