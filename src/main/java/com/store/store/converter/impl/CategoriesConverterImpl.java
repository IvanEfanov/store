package com.store.store.converter.impl;

import com.store.store.converter.CategoriesConverter;
import com.store.store.dto.CategoriesData;
import com.store.store.entity.Categories;
import org.springframework.stereotype.Component;


@Component
public class CategoriesConverterImpl implements CategoriesConverter {


    public CategoriesData converter(Categories categories)
    {
        CategoriesData categoriesData = new CategoriesData();
        categoriesData.setCategoriesID(categories.getCategoriesID());
        categoriesData.setName(categories.getName());
        categoriesData.setGoods(categories.getGoods());


        return categoriesData;
    }

}
