package com.store.store.converter.impl;

import com.store.store.converter.CommentConverter;
import com.store.store.dto.CommentsData;
import com.store.store.entity.Comments;
import org.springframework.stereotype.Component;

@Component
public class CommentConverterImpl implements CommentConverter {

    public CommentsData converter(Comments comments)
    {
        CommentsData commentsData = new CommentsData();
        commentsData.setCommentID(comments.getCommentID());
        commentsData.setValue(comments.getValue());
        commentsData.setUser(comments.getUser());
        commentsData.setGood(comments.getGood());

        return commentsData;
    }
}
