package com.store.store.converter.impl;


import com.store.store.converter.UserConverter;
import com.store.store.dto.UserData;
import com.store.store.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserConverterImpl implements UserConverter{

    public UserData converter(User user)
    {
        UserData userData = new UserData();

        userData.setLogin(user.getLogin());
        userData.setName(user.getName());
        userData.setPassword(user.getPassword());
        userData.setSurname(user.getSurname());
        userData.setAddress(user.getAddress());
        userData.setCity(user.getCity());
        userData.setEmail(user.getEmail());
        userData.setComments(user.getComments());

        return userData;
    }
}
