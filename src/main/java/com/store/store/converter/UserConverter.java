package com.store.store.converter;

import com.store.store.dto.UserData;
import com.store.store.entity.User;

public interface UserConverter {

    UserData converter(User user);
}
