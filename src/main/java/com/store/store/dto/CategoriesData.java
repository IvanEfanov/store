package com.store.store.dto;

import com.store.store.entity.Good;

import java.util.Set;

public class CategoriesData {

    private int categoriesID;
    private String name;
    private Set<Good> goods;

    public Set<Good> getGoods() {
        return goods;
    }

    public void setGoods(Set<Good> goods) {
        this.goods = goods;
    }

    public int getCategoriesID() {
        return categoriesID;
    }

    public void setCategoriesID(int categoriesID) {
        this.categoriesID = categoriesID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
