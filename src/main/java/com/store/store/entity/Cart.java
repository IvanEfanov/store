package com.store.store.entity;



import javax.persistence.*;
import javax.persistence.Table;

@Entity
@Table(name = "Cart")
public class Cart {


    @Id
    @Column(name = "ID")
    @GeneratedValue
    private int cartID;

    @GeneratedValue
    @Column(name = "orderID")
    private int orderID;

    @Column(name = "userID")
    private int userID;

    public int getCartID() {
        return cartID;
    }

    public void setCartID(int cartID) {
        this.cartID = cartID;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}


