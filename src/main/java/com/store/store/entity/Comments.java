package com.store.store.entity;


import javax.persistence.*;


@Entity
@Table(name = "Comments")
public class Comments {

    @Id
    @GeneratedValue
    @Column(name = "commentID")
    private int commentID;

    @Column(name = "value")
    private String value;

    @ManyToOne(targetEntity = Good.class, fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name ="good",referencedColumnName = "goodID")
    private Good good;

    @ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name ="user",referencedColumnName = "login")
    private User user;

    public int getCommentID() {
        return commentID;
    }

    public void setCommentID(int commentID) {
        this.commentID = commentID;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Good getGood() {
        return good;
    }

    public void setGood(Good good) {
        this.good = good;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
