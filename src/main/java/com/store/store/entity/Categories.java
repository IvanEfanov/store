package com.store.store.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Categories")
public class Categories {

    @Id
    @GeneratedValue
    @Column(name = "categoriesID")
    private int categoriesID;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "categories")
    private Set<Good> goods = new HashSet<>();

    public Set<Good> getGoods() {
        return goods;
    }

    public void setGoods(Set<Good> goods) {
        this.goods = goods;
    }

    public int getCategoriesID() {
        return categoriesID;
    }

    public void setCategoriesID(int categoriesID) {
        this.categoriesID = categoriesID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
