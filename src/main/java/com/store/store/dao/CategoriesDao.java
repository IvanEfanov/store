package com.store.store.dao;


import com.store.store.entity.Categories;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface CategoriesDao {

    List<Categories> getCategories();

    void saveCategory(String name);

    Categories getCategoryByID(int categoryID);
}
