package com.store.store.dao;

import com.store.store.entity.Categories;
import com.store.store.entity.Good;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface GoodDao {

    List<Good> getAllGoods();

    Good getGoodByID(int goodID);

    List<Good> goodByCategory(int categoriesID);

    void saveGood(String name, int price, String description, Categories categories);
}
