package com.store.store.dao;


import com.store.store.entity.User;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface UserDao {

        void setUser(String login, String password, String name, String surname, String email, String city, String address);

        User findByLogin(String login);
}
