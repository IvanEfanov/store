package com.store.store.dao.impl;

import com.store.store.dao.GoodDao;
import com.store.store.entity.Categories;
import com.store.store.entity.Good;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
@Transactional
public class GoodDaoImpl implements GoodDao {


    @Autowired
    JdbcTemplate template;
    @Autowired
    EntityManager entityManager;

    public List<Good> getAllGoods() {

        return template.query("SELECT G.GOODID, G.NAME, G.PRICE, G.DESCRIPTION " +
                "FROM good G ", new RowMapper<Good>() {
            @Override
            public Good mapRow(ResultSet resultSet, int i) throws SQLException {
                Good good = new Good();
                good.setGoodID(resultSet.getInt(1));
                good.setName(resultSet.getString(2));
                good.setPrice(resultSet.getDouble(3));
                good.setDescription(resultSet.getString(4));
                return good;
            }
        });
    }

    public Good getGoodByID(int goodID) {
        return entityManager.find(Good.class, goodID);
    }


    public List<Good> goodByCategory(int categoriesID) {
        Categories categories = entityManager.find(Categories.class, categoriesID);

        return template.query("SELECT G.GOODID, G.NAME, G.PRICE, G.DESCRIPTION, G.categories " +
                "FROM good G " +
                "WHERE categories =" + categories.getCategoriesID(), new RowMapper<Good>() {
            @Override
            public Good mapRow(ResultSet resultSet, int i) throws SQLException {
                Good good = new Good();
                good.setGoodID(resultSet.getInt(1));
                good.setName(resultSet.getString(2));
                good.setPrice(resultSet.getInt(3));
                good.setDescription(resultSet.getString(4));
                good.setCategories(categories);
                return good;
            }
        });
    }


    public void saveGood(String name, int price, String description, Categories categories)
    {
        Good good = new Good();
        good.setName(name);
        good.setPrice(price);
        good.setDescription(description);
        good.setCategories(categories);
        entityManager.persist(good);
    }

}
