package com.store.store.dao.impl;

import com.store.store.dao.CategoriesDao;
import com.store.store.entity.Categories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
@Transactional
public class CategoriesDaoImpl implements CategoriesDao {

    @Autowired
    JdbcTemplate template;
    @Autowired
    EntityManager entityManager;

    public List<Categories> getCategories()
    {
        return template.query("SELECT C.categoriesid, C.name " +
                "FROM categories C ", new RowMapper<Categories>() {
            @Override
            public Categories mapRow(ResultSet resultSet, int i) throws SQLException {
                Categories categories = new Categories();
                categories.setCategoriesID(resultSet.getInt(1));
                categories.setName(resultSet.getString(2));
                return categories;
            }

        });
    }

    public void saveCategory(String name)
    {
        Categories categories = new Categories();
        categories.setName(name);
        entityManager.persist(categories);

    }



    public Categories getCategoryByID(int categoryID)
    {
        return entityManager.find(Categories.class, categoryID);
    }
}
