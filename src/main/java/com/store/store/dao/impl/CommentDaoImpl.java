package com.store.store.dao.impl;


import com.store.store.dao.CommentDAO;
import com.store.store.dao.UserDao;
import com.store.store.entity.Comments;
import com.store.store.entity.Good;
import com.store.store.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
@Transactional
public class CommentDaoImpl implements CommentDAO{

    @Autowired
    EntityManager entityManager;
    @Autowired
    JdbcTemplate template;
    @Autowired
    UserDao userDao;

    public List<Comments> getCommentsByGood(int goodID)
    {
        Good good = entityManager.find(Good.class,goodID);
        return template.query("SELECT C.commentid, C.VALUE, C.USER, C.GOOD " +
                "FROM comments C " +
                "WHERE good = " + goodID, new RowMapper<Comments>() {
            @Override
            public Comments mapRow(ResultSet resultSet, int i) throws SQLException {
                Comments comment = new Comments();
                comment.setCommentID(resultSet.getInt(1));
                comment.setValue(resultSet.getString(2));
                comment.setUser(userDao.findByLogin(resultSet.getString(3)));
                comment.setGood(good);
                return comment;
            }
        });
    }


    public void addNewComment(String comment, int goodID)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();

        User user = userDao.findByLogin(name);
        Good good = entityManager.find(Good.class,goodID);

        Comments newComment = new Comments();
        newComment.setValue(comment);
        newComment.setGood(good);
        newComment.setUser(user);

        entityManager.persist(newComment);

    }
}
