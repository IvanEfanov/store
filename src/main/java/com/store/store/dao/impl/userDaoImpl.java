package com.store.store.dao.impl;

import com.store.store.dao.UserDao;
import com.store.store.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class userDaoImpl implements UserDao {

    @Autowired
    EntityManager entityManager;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    public void setUser(String login, String password, String name, String surname, String email, String city, String address)
    {
        String pass = bCryptPasswordEncoder.encode(password);
        User user = new User();
        user.setLogin(login);
        user.setPassword(pass);
        user.setName(name);
        user.setSurname(surname);
        user.setEmail(email);
        user.setCity(city);
        user.setAddress(address);

        entityManager.persist(user);

    }

    public User findByLogin(String login)
    {
        return  entityManager.find(User.class, login);
    }
}
