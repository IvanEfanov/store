package com.store.store.dao;

import com.store.store.entity.Comments;

import java.util.List;

public interface CommentDAO {
    List<Comments> getCommentsByGood( int goodID);

    void addNewComment(String comment, int goodID);
}
