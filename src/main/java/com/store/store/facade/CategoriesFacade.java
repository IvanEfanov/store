package com.store.store.facade;

import com.store.store.dto.CategoriesData;

import java.util.List;

public interface CategoriesFacade {

    List<CategoriesData> getCategoriesList();

    CategoriesData categoryByID(int categoryID);
}
