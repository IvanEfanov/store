package com.store.store.facade.impl;

import com.store.store.converter.CommentConverter;
import com.store.store.dto.CommentsData;
import com.store.store.entity.Comments;
import com.store.store.facade.CommentsFacade;
import com.store.store.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class CommentsFacadeImpl implements CommentsFacade {

    @Autowired
    CommentService commentService;
    @Autowired
    CommentConverter commentConverter;

    public List<CommentsData> getCommentsByGood(int goodID)
    {
        List<CommentsData> convertList = new ArrayList<>();
        List<Comments> list = commentService.commentByGood(goodID);

        Iterator<Comments> iterator = list.iterator();
        while(iterator.hasNext())
        {
            convertList.add(commentConverter.converter(iterator.next()));
        }

        return convertList;
    }


}
