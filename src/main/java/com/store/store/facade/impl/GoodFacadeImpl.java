package com.store.store.facade.impl;

import com.store.store.converter.GoodConverter;
import com.store.store.dto.GoodData;
import com.store.store.entity.Good;
import com.store.store.facade.GoodFacade;
import com.store.store.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Component
public class GoodFacadeImpl implements GoodFacade {

    @Autowired
    GoodConverter goodConverter;
    @Autowired
    GoodService goodService;

    public List<GoodData> allGoods()
    {
        List<GoodData> convertList = new ArrayList<>();
        List<Good> list = goodService.allGoods();

        Iterator<Good> iterator = list.iterator();
        while(iterator.hasNext())
        {
            convertList.add(goodConverter.converter(iterator.next()));
        }

        return convertList;
    }

    public GoodData goodById(int goodID)
    {
        return goodConverter.converter(goodService.goodById(goodID));
    }

    public List<GoodData> goodByCategories(int categoriesID)
    {
        List<GoodData> convertList = new ArrayList<>();
        List<Good> list = goodService.goodByCategories(categoriesID);

        Iterator<Good> iterator = list.iterator();
        while(iterator.hasNext())
        {
            convertList.add(goodConverter.converter(iterator.next()));
        }

        return convertList;
    }

}
