package com.store.store.facade.impl;

import com.store.store.converter.CategoriesConverter;
import com.store.store.dto.CategoriesData;
import com.store.store.entity.Categories;
import com.store.store.facade.CategoriesFacade;
import com.store.store.service.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class CategoriesFacadeImpl implements CategoriesFacade {

    @Autowired
    CategoriesService categoriesService;

    @Autowired
    CategoriesConverter categoriesConverter;

    public List<CategoriesData> getCategoriesList()
    {
        List<CategoriesData> convertList = new ArrayList<>();
        List<Categories> list = categoriesService.getCategoriesList();

        Iterator<Categories> iterator = list.iterator();

        while(iterator.hasNext())
        {
            convertList.add(categoriesConverter.converter(iterator.next()));
        }

        return convertList;
    }

    public CategoriesData categoryByID(int categoryID)
    {
        return categoriesConverter.converter(categoriesService.categoryByID(categoryID));
    }
}
