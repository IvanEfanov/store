package com.store.store.facade;

import com.store.store.dto.GoodData;

import java.util.List;

public interface GoodFacade {

         List<GoodData> allGoods();

        GoodData goodById(int goodID);

        List<GoodData> goodByCategories(int categoriesID);
}
