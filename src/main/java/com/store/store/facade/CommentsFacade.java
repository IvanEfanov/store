package com.store.store.facade;

import com.store.store.dto.CommentsData;

import java.util.List;

public interface CommentsFacade {

    List<CommentsData> getCommentsByGood(int goodID);
}
