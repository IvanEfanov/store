INSERT INTO storefont.user (login, address, city, email, name, password, surname) VALUES ('admin','admin','admin', 'admin',
    'admin', '$2a$10$SoMiInP4.IclUfwlvIbweOPofa70uK8uaLdNhW3LF25Yza2fJu5w.', 'admin');

INSERT INTO storefont.categories (categoriesid,name) VALUES (1,'category 1');
INSERT INTO storefont.categories (categoriesid,name) VALUES (2,'category 2');
INSERT INTO storefont.categories (categoriesid,name) VALUES (3,'category 3');
INSERT INTO storefont.categories (categoriesid,name) VALUES (4,'category 4');
INSERT INTO storefont.categories (categoriesid,name) VALUES (5,'category 5');
INSERT INTO storefont.categories (categoriesid,name) VALUES (6,'category 6');

INSERT INTO storefont.good (goodid,name, price, description, categories) VALUES (1,'good1', '23.0', 'test', 2);
INSERT INTO storefont.good (goodid,name, price, description, categories ) VALUES (2,'good2', '23.0', 'test', 3);
INSERT INTO storefont.good (goodid,name, price, description, categories) VALUES (3,'good3', '23.0', 'test', 3);
INSERT INTO storefont.good (goodid,name, price, description, categories) VALUES (4,'good4', '23.0', 'test', 5);
INSERT INTO storefont.good (goodid,name, price, description, categories) VALUES (5,'good5', '23.0', 'test', 6);
INSERT INTO storefont.good (goodid,name, price, description, categories) VALUES (6,'good6', '23.0', 'test', 1);
INSERT INTO storefont.good (goodid,name, price, description, categories) VALUES (7,'good7', '23.0', 'test', 4);
INSERT INTO storefont.good (goodid,name, price, description, categories) VALUES (8,'good8', '23.0', 'test', 4);
INSERT INTO storefont.good (goodid,name, price, description, categories) VALUES (9,'good9', '23.0', 'test', 1);

INSERT INTO storefont.comments (commentID, value, good, user) VALUES (1,'Nice quality', 1, 'admin');
INSERT INTO storefont.comments (commentID, value, good, user) VALUES (2,'Good!', 1, 'admin');
INSERT INTO storefont.comments (commentID, value, good, user) VALUES (3,'Nice quality', 4, 'admin');
INSERT INTO storefont.comments (commentID, value, good, user) VALUES (4,'Nice quality', 5, 'admin');
INSERT INTO storefont.comments (commentID, value, good, user) VALUES (5,'Nice quality', 2, 'admin');
INSERT INTO storefont.comments (commentID, value, good, user) VALUES (6,'STFU AND TAKE MY MONEY!', 2, 'admin');

