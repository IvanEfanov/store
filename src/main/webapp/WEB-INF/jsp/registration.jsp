<%@page contentType="text/html; encoding=utf-8"%>
<html>
<html>

    <head>
        <title>Registration</title>
        <style>

            #registrationstyle{
                width: 320px;
                padding: 30px;
                margin: 150px auto;
                background: lavender;
                -webkit-border-radius: 2px;
                -moz-border-radius: 2px;
                border: 1px solid #000;
            }
            .but
            {
                width: 150px;
                height: 30px;
            }
            .text
            {
                width: 300px;
                padding: 7px;
                margin: 5px;
            }
        </style>
    </head>
    <body bgcolor="#b0c4de">
    <div id="registrationstyle" >
    <h2>Please enter your personal info</h2>
        <hr>
        <div>
            <form method="post" action="/login">
                <input  type="text" name="login" placeholder="login" class="text" id="login">
                <input type="text" name="password" placeholder="Password" class="text" id="password">
                <input type="text" name="name" placeholder="name" class="text" id="name">
                <input type="text" name="surname" placeholder="Surname" class="text" id="surname">
                <input type="text" name="email" placeholder="Email" class="text" id="email">
                <input type="text" name="city" placeholder="City" class="text" id="city">
                <input type="text" name="address" placeholder="Address" class="text" id="address">
                <hr>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                <input type="submit" name="registration" value="Create account" class="but">
                <input type="submit" name="cancel" value="Cancel" class="but">
            </form>
        </div>

    </div>
    </body>
</html>