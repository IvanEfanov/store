<%@page contentType="text/html; encoding=utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<html>
<html>

<head>
    <title>Welcome</title>
    <style>
        #table
        {
            width: 800px;
            height: 750px;
            margin-left: 90px;

        }
        th,td{
            padding: 2px;
            border: 1px solid #000000;
            text-align: center;
        }
        #registrationstyle{
            width: 320px;
            padding: 30px;
            margin-left: 75%;
            margin-top: 70px;
            background: lavender;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 1px solid #000;
        }

        #loginstyle{
            width: 320px;
            padding: 30px;
            margin-left: 75%;
            margin-top: 25px;
            background: lavender;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 1px solid #000;
        }
        #content{
            width: 1000px;
            height: auto;
            padding: 30px;
            margin-left:17%;
            margin-top: -490px;
            background: aliceblue;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 1px solid #000;
        }
        .but
        {
            width: 140px;
            height: 30px;
        }
        .headbut
        {
            width: 200px;
            height: 50px;
            text-align: center;
            font-size: 20px;
            background-color:lightblue;
            border-color: steelblue;
        }
        .tablebut
        {
            margin-left: 40px;
        }
        .td2
        {
            border: hidden;
        }
    </style>
</head>
<body bgcolor="#b0c4de">
<div id="loginstyle">
    <h2>Log in</h2>
    <hr>
    <div>
        <form action="/j_spring_security_check" method="post">
            <input type="text" placeholder="login" name="login">
            <input type="text" placeholder="password" name="password">
            <hr>
            <input class="but" type="submit" name="Login" value="Log In">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
    </div>

</div>

<div id="registrationstyle">
    <h3>Create a new account</h3>
    <hr>
    <div>
        <form method="get" action="/registration">
            <input type="submit" name="registration" value="Registration" class="but">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
    </div>

</div>

<div id="content">
    <table class="tablebut">
        <tr>
            <form action="/auth" method="get">
                <input class="headbut" type="submit" name="categories" value="categories">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
            <form>
                <input class="headbut" type="submit" name="new" value="new">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
            <form>
                <input class="headbut" type="submit" name="sellout" value="sellout">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
            <form>
                <input  class="headbut" type="submit" name="search" value="search">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
            <form action="main" method="get">
                <input  class="headbut" type="submit" name="main" value="main">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>

            </td>
        </tr>

    </table>

    <hr>
    <table id="table">
        <c:forEach items="${goods}" var="item">
            <tr>
                <td>
                    Image:
                </td>
                <td>Name: ${item.name}<br>
                Price: ${item.price}<br>
                Description: ${item.description}</td>
                <td>

                    <form action="/auth" method="get">
                        <input type="submit" name="details" value="Details">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    </form>

                </td>
            </tr>
        </c:forEach>
    </table>
</div>


</body>
</html>