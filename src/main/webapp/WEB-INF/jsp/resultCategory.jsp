<%@page contentType="text/html; encoding=utf-8"%>
<html>
<html>

<head>
    <title>Result</title>
    <style>

        #resultstyle{
            width: 320px;
            padding: 30px;
            margin: 150px auto;
            background: lavender;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 1px solid #000;
        }
        .but
        {
            width: 150px;
            height: 30px;
        }
        .text
        {
            width: 300px;
            padding: 7px;
            margin: 5px;
        }
    </style>
</head>
<body bgcolor="#b0c4de">
<div id="resultstyle" >
    <h2>A new category has been added!</h2>
    <hr>
    <div>
        <table>
            <tr><td><h3>Add one more?</h3></td></tr>
            <tr>
            <td>
                <form method="post" action="/categories/edit">
                    <input type="hidden" name="asname" value="${asname}">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <input type="submit" name="addCategory" value="Add more" class="but">
                </form>
            </td>
            <td>
                <form action="/categories" method="post">
                    <input type="submit" name="cancel" value="Cancel" class="but">
                    <input type="hidden" name="asname" value="${asname}">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                </form>
            </td>
            </tr>

        </table>
    </div>


</div>
<hr>
</body>
</html>