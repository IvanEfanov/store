<%@page contentType="text/html; encoding=utf-8"%>
<html>
<html>

<head>
    <title>Registration</title>
    <style>

        #registrationstyle{
            width: 320px;
            padding: 30px;
            margin: 150px auto;
            background: lavender;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 1px solid #000;
        }
        .but
        {
            width: 150px;
            height: 30px;
        }
    </style>
</head>
<body bgcolor="#b0c4de">
<div id="registrationstyle">
    <h2>Log in to continue</h2>
    <hr>
    <div>
        <form action="/j_spring_security_check" method="post">
            <input type="text" placeholder="login" name="login">
            <input type="text" placeholder="password" name="password">
            <hr>
            <input class="but" type="submit" name="Login" value="Log In">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <hr>

        </form>
    </div>
            <div>
                <form action="/registration" method="get">
                <h3>Or create a new account</h3></p>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                <input type="submit" name="registration" value="Create account" class="but">
                </form>
                <form action="/login" method="get">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <input type="submit" name="cancel" value="Cancel" class="but">
                </form>


    </div>
</div>
</body>
</html>