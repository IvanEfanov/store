<%@page contentType="text/html; encoding=utf-8"%>
<html>
<html>

<head>
    <title>Registration</title>
    <style>

        #registrationstyle{
            width: 320px;
            padding: 30px;
            margin: 150px auto;
            background: lavender;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 1px solid #000;
        }
        .but
        {
            width: 150px;
            height: 30px;
        }
        .text
        {
            width: 300px;
            padding: 7px;
            margin: 5px;
        }
    </style>
</head>
<body bgcolor="#b0c4de">
<div id="registrationstyle" >
    <h2>Please, name a new category!</h2>
    <hr>
    <div>
        <form method="post" action="/categories/result">
            <input  type="text" name="nameCategory" placeholder="name" class="text" id="nameCategory">
            <hr>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <input type="submit" name="addCategory" value="Add a new category" class="but">
            <input type="hidden" name="asname" value="${asname}">

        </form>
        <form action="/categories" method="post">
            <input type="submit" name="cancel" value="Cancel" class="but">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <input type="hidden" name="asname" value="${asname}">
        </form>
    </div>

</div>
</body>
</html>