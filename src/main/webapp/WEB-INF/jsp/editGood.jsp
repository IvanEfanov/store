<%@page contentType="text/html; encoding=utf-8"%>
<html>
<html>

<head>
    <title>Edit good</title>
    <style>

        #registrationstyle{
            width: 320px;
            padding: 30px;
            margin: 150px auto;
            background: lavender;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 1px solid #000;
        }
        .but
        {
            width: 150px;
            height: 30px;
        }
        .text
        {
            width: 300px;
            padding: 7px;
            margin: 5px;
        }
    </style>
</head>
<body bgcolor="#b0c4de">
<div id="registrationstyle" >
    <h2>Please, enter info about new good!</h2>
    <hr>
    <div>
        <form method="post" action="/good/edit/result">
            <input  type="text" name="name" placeholder="name" class="text" id="name">
            <input  type="text" name="price" placeholder="price" class="text" id="price">
            <input  type="text" name="description" placeholder="description" class="text" id="description">
            <input  type="hidden" name="categoriesID" value="${categoriesID}">
            <input type="hidden" name="asname" value="${asname}">
            <hr>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <input type="submit" name="addGood" value="Add a new good" class="but">

        </form>
        <form action="/goodsByCategory" method="post">
            <input  type="hidden" name="categoriesID" value="${categoriesID}">
            <input type="hidden" name="asname" value="${asname}">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <input type="submit" name="cancel" value="Cancel" class="but">
        </form>
    </div>

</div>
</body>
</html>