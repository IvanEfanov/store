<%@page contentType="text/html; encoding=utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<html>

<head>
    <title>Details</title>
    <style>
        #comments
        {
            width: 800px;
            height: 250px;
            margin-left: 90px;
        }
        #table
        {
            width: 800px;
            height: 750px;
            margin-left: 90px;

        }
        th,td{
            padding: 2px;
            border: 1px solid #000000;
            text-align: center;
        }

        #cartstyle{
            width: 320px;
            padding: 30px;
            margin-left: 75%;
            margin-top: 30px;
            background: lavender;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 1px solid #000;
        }

        #logoutstyle{
            width: 320px;
            padding: 30px;
            margin-left: 75%;
            margin-top: 25px;
            background: lavender;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 1px solid #000;
        }
        #content{
            width: 1000px;
            height: auto;
            padding: 30px;
            margin-left:17%;
            margin-top: -400px;
            background: aliceblue;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 1px solid #000;
        }
        #menu{
            width: 210px;
            height: auto;
            padding: 30px;
            margin-left:1%;
            margin-top: -1100px;
            background: lavender;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 1px solid #000;
        }
        .but
        {
            width: 140px;
            height: 30px;
        }
        .but2
        {
            width: 210px;
            height: 20px;
        }
        .headbut
        {
            width: 200px;
            height: 50px;
            text-align: center;
            font-size: 20px;
            background-color:lightblue;
            border-color: steelblue;
        }
        .tablebut
        {
            margin-left: 40px;
        }
        .comment
        {
            width: 800px;
            height: 100px;
            margin-left: 40px;
        }
        .sendbut
        {
            margin-left: 80%;
            width: 150px;
            height: 40px;
            text-align: center;
            font-size: 18px;

        }

    </style>
</head>
<body bgcolor="#b0c4de">
<div id="logoutstyle">
    <h2>Logout</h2>
    <hr>
    In system as: <b>${asname}</b>
    <hr>
    <div>
        <form action="/logout" method="post">

            <input class="but" type="submit" name="logout" value="logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
    </div>

</div>

<div id="cartstyle">
    <h3>Your cart</h3>
    <hr>
    <div>
        <form method="get" action="/cart">
            <input type="submit" name="cart" value="cart" class="but">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
    </div>

</div>

<div id="content">
    <table class="tablebut">
        <tr>
            <form action="/categories" method="post">
                <input class="headbut" type="submit" name="categories" value="categories">
                <input type="hidden" name="asname" value="${asname}">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
            <form>
                <input class="headbut" type="submit" name="new" value="new">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
            <form>
                <input class="headbut" type="submit" name="sellout" value="sellout">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
            <form>
                <input  class="headbut" type="submit" name="search" value="search">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
            <form action="main" method="get">
                <input  class="headbut" type="submit" name="main" value="main">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
        </tr>

    </table>

    <table id="table">
        <tr>
            <td>
                Image:
            </td>
            <td>
                <form action="/cart" method="post">
                    <input type="hidden" name="goodID" value="${good.goodID}">

                    <input class="but" type="submit" name="addToCart" value="Add to cart">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                </form>
            </td>
        </tr>
        <tr>
            <td>
                Name:${good.name}
                price:${good.price}
                Description:${good.description}
            </td>

        </tr>

    </table>

    <hr>
    <div>
        <form action="/comment" method="post">
            <div class="comment">
                <input  type="text" name="comment" placeholder="comment" class="comment" id="comment">
                <input type="hidden" name="goodID" value="${good.goodID}">
                <input type="hidden" name="asname" value="${asname}">
            </div>
            <br>
               <input class="sendbut" type="submit" name="sendComment" value="SendComment">
               <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
    </div>
    <hr>
    <div>
        <table id="comments">
        <c:forEach items="${comments}" var="item">

                <tr>
                    <td>${item.user.login}</td>
                    <td>${item.value}</td>
                </tr>

        </c:forEach>
        </table>
    </div>
</div>


<div id="menu">
    <h3>menu</h3>
    <hr>
    <input class="but2" type="submit" name="option1" value="option1">
    <hr>
    <input class="but2" type="submit" name="option2" value="option2">
    <hr>
    <input class="but2" type="submit" name="option3" value="option3">
    <hr>
    <input class="but2" type="submit" name="option4" value="option4">
    <hr>
    <input class="but2" type="submit" name="option5" value="option5">
    <hr>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</div>
</body>
</html>