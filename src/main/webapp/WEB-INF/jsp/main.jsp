<%@page contentType="text/html; encoding=utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <title>Main</title>
    <style>
        #table
        {
            width: 800px;
            height: 750px;
            margin-left: 90px;

        }
        th,td{
            padding: 2px;
            border: 1px solid #000000;
            text-align: center;
        }

        #cartstyle{
            width: 320px;
            padding: 30px;
            margin-left: 75%;
            margin-top: 30px;
            background: lavender;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 1px solid #000;
        }

        #logoutstyle{
            width: 320px;
            padding: 30px;
            margin-left: 75%;
            margin-top: 25px;
            background: lavender;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 1px solid #000;
        }
        #content{
            width: 1000px;
            height: auto;
            padding: 30px;
            margin-left:17%;
            margin-top: -400px;
            background: aliceblue;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 1px solid #000;
        }
        #menu{
            width: 210px;
            height: auto;
            padding: 30px;
            margin-left:1%;
            margin-top: -883px;
            background: lavender;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 1px solid #000;
        }
        .but
        {
            width: 140px;
            height: 30px;
        }
        .but2
        {
            width: 210px;
            height: 20px;
        }
        .headbut
        {
            width: 200px;
            height: 50px;
            text-align: center;
            font-size: 20px;
            background-color:lightblue;
            border-color: steelblue;
        }
        .tablebut
        {
            margin-left: 40px;
        }
        .td2
        {
            border: hidden;
        }
    </style>
</head>
<body bgcolor="#b0c4de">
<div id="logoutstyle">
    <h2>Logout</h2>
    <hr>
    In system as: <b>${asname}</b>
    <hr>
    <div>
        <form action="/logout" method="post">

            <input class="but" type="submit" name="logout" value="logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
    </div>

</div>

<div id="cartstyle">
    <h3>Your cart</h3>
    <hr>
    <div>
        <form method="get" action="/cart">
            <input type="submit" name="cart" value="cart" class="but">

        </form>
    </div>

</div>

<div id="content">
    <table class="tablebut">
        <tr>
            <form action="/categories" method="post">
                <input class="headbut" type="submit" name="categories" value="categories">
                <input type="hidden" name="asname" value="${asname}">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
            <form>
                <input class="headbut" type="submit" name="new" value="new">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
            <form>
                <input class="headbut" type="submit" name="sellout" value="sellout">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
            <form>
                <input  class="headbut" type="submit" name="search" value="search">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
            <form action="main" method="get">
                <input  class="headbut" type="submit" name="main" value="main">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>

            </td>
        </tr>

    </table>

    <hr>
    <table id="table">
        <c:forEach items="${goods}" var="item">
            <tr>
                <td>
                    Image:
                </td>
                <td>Name: ${item.name}<br>
                    Price: ${item.price}<br>
                    Description: ${item.description}</td>
                <td>
                    <form action="/goodDetails" method="post">
                        <input type="hidden" name="goodID" value="${item.goodID}">
                        <input type="hidden" name="asname" value="${asname}">
                        <input type="submit" name="details" value="Details">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>


</div>

<div id="menu">
    <h3>menu</h3>
    <hr>
    <input class="but2" type="submit" name="option1" value="option1">
    <hr>
    <input class="but2" type="submit" name="option2" value="option2">
    <hr>
    <input class="but2" type="submit" name="option3" value="option3">
    <hr>
    <input class="but2" type="submit" name="option4" value="option4">
    <hr>
    <input class="but2" type="submit" name="option5" value="option5">
    <hr>
</div>

</body>
</html>